package es.pildoras.spring.mvc;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import es.pildoras.spring.mvc.validacionesPersonalizadas.CPostalMadrid;


public class Alumno {

	@NotNull
	@Size(min=2, message=" Error en el tama�o del nombre ")
	private String nombre;
	
	@Min(value=10, message="Error, edad menor a 10 a�os")
	@Max(value=100, message="Error, edad mayor a 100 a�os")
	private Integer edad;
	
	private String apellido;
	
	private String optativa;
	
	private String ciudadEstudio;
	
	private String idiomaEscogido;
	
	@NotNull
	@Email
	private String email;
	
	//se permiten valores numericos y de tama�o 5, si quieres poner tambien letras seria [0-9a-z]
	//@NotNull
	//@Pattern(regexp = "[0-9]{5}", message = "Valores no validos, 5 caracteres del 0 al 9")
	@CPostalMadrid
	private String codigoPostal;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getOptativa() {
		return optativa;
	}

	public void setOptativa(String optativa) {
		this.optativa = optativa;
	}

	public String getCiudadEstudio() {
		return ciudadEstudio;
	}

	public void setCiudadEstudio(String ciudadEstudio) {
		this.ciudadEstudio = ciudadEstudio;
	}

	public String getIdiomaEscogido() {
		return idiomaEscogido;
	}

	public void setIdiomaEscogido(String idiomaEscogido) {
		this.idiomaEscogido = idiomaEscogido;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

}
