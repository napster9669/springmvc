package es.pildoras.spring.mvc.validacionesPersonalizadas;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CPostalMadridValidacion implements ConstraintValidator<CPostalMadrid, String> {

	String prefijoCodigoMadrid;
	
	@Override
	public void initialize(CPostalMadrid elCodigo) {
		prefijoCodigoMadrid = elCodigo.value();
	}
	
	@Override
	public boolean isValid(String arg0, ConstraintValidatorContext arg1) {

		boolean valCodigo = false;
		
		if(arg0 != null && arg0.startsWith(prefijoCodigoMadrid)) {
			valCodigo = true;
		}
		
		return valCodigo;
	}

}
