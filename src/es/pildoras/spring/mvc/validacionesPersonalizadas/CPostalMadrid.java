package es.pildoras.spring.mvc.validacionesPersonalizadas;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = CPostalMadridValidacion.class) //Futura clase que contendr� la logica de la validacion
@Target({ElementType.METHOD, ElementType.FIELD}) // la validacion puede ir tanto a metodos como a campos 
@Retention(RetentionPolicy.RUNTIME) // cuando se quiere que se chequee la validacion (en tiempo de ejecuci�n en nuestro caso)
public @interface CPostalMadrid {

	//Definir el c�digo postal por defecto
	public String value() default "28";
	
	//Definir el mensaje de error
	public String message() default "El c�digo postal debe comenzar por 28";
	
	//Definir los grupos, 
	Class<?>[] groups() default {};
	
	//Definir los payloads
	Class<? extends Payload>[] payload() default {};
	
}
