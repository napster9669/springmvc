package es.pildoras.spring.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/principal")
public class HolaAlumnosControlador {

	@RequestMapping("/muestraFormulario")
	public String muestraFormulario() {//proporciona el formulario 
		return "HolaAlumnosFormulario";
	}
	
	@RequestMapping("/procesarFormulario")
	public String procesarFormulario() {
		return "HolaAlumnosSpring";
	}
	
	//RequestParam obtenemos el nombre del JSP del formulario
	@RequestMapping("/procesarFormulario2")
	//ublic String otroProcesoFormulario(HttpServletRequest request, Model modelo) {
	public String otroProcesoFormulario(@RequestParam("nombreAlumno")String nombre, Model modelo) {
		//String nombre = request.getParameter("nombreAlumno");
		
		nombre+="es el mejor alumno";
		
		String finalMensaje = "�Quien es el mejor alumno?" + ": " + nombre;
		
		modelo.addAttribute("mensajeFinal", finalMensaje);
		
		return "HolaAlumnosSpring";
	}
	
}
