package es.pildoras.spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TirarControlador {

	@RequestMapping("/procesarFormulario2")
	//ublic String otroProcesoFormulario(HttpServletRequest request, Model modelo) {
	public String otroProcesoFormulario(@RequestParam("nombreAlumno")String nombre, Model modelo) {
		//String nombre = request.getParameter("nombreAlumno");
		
		nombre+="es el peor alumno";
		
		String finalMensaje = "�Quien es el peor alumno?" + ": " + nombre;
		
		modelo.addAttribute("mensajeFinal", finalMensaje);
		
		return "HolaAlumnosSpring";
	}
	
}
