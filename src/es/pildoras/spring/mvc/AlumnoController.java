package es.pildoras.spring.mvc;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/alumno")
public class AlumnoController {

	
	@RequestMapping("/muestraFormulario")
	public String muestraFormulario(Model modelo) {
		
		Alumno alumno1 = new Alumno();
		modelo.addAttribute("alumno1", alumno1);
		
		return "AlumnoRegistroFormulario";
	}
	
	//el Binding result tiene que ir despu�s del objeto que se va a comprobar, en este caso alumno1
	@RequestMapping("/procesarFormulario")
	public String procesarFormulario(@Valid @ModelAttribute("alumno1") Alumno alumno1, BindingResult resultadoValidacion, Model modelo){
		
		if(resultadoValidacion.hasErrors()) 
			return "AlumnoRegistroFormulario";
		
		String nombreCompleto = alumno1.getNombre() + " " + alumno1.getApellido();
		
		modelo.addAttribute("nombreCompleto", nombreCompleto);
		
		
		return "AlumnoProcesarFormulario";
	}
	
	
	//Creamos el Binder para controlar que no existan espacios en blanco en el nombre, que es el que controlamos que no sea null
	//Funciona para todos los atributos en los que controlamos lo que introducimos 
	@InitBinder
	public void newBinder(WebDataBinder binder) {
		
		StringTrimmerEditor recortaEspaciosBlancos = new StringTrimmerEditor(true);
		
		binder.registerCustomEditor(String.class, recortaEspaciosBlancos);
		
	}
	
	
}
