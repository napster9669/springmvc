<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Formulario de registro</title>
</head>
<body>

	<form:form action="procesarFormulario" modelAttribute="alumno1">
	
	<!-- el nombre del path tiene que ser el mismo que lo que esta puesto a continuacion de los 
	 getter y setter pero con minuscula -->
	 
	Nombre:
	<form:input path="nombre"/>
	<form:errors path="nombre" style="color:red"></form:errors>
	
	Apellido:
	<form:input path="apellido"/>
	<br/><br/>
	Asignaturas optativas:<br/>
	<form:select path="optativa" multiple="true">
		<form:option value="Dise�o" label="Dise�o"/>
		<form:option value="Karate" label="Karate"/>
		<form:option value="Comecio" label="Comercio"/>
		<form:option value="Danza" label="Danza"/>
	</form:select>
	
	<br/><br/><br/>
	
		Barcelona<form:radiobutton path="ciudadEstudio" value="Barcelona"/>
		Sevilla<form:radiobutton path="ciudadEstudio" value="Sevilla"/>
		Granada<form:radiobutton path="ciudadEstudio" value="Granada"/>
		
	<br/><br/><br/>	
	
		Ingles<form:checkbox path="idiomaEscogido" value="Ingles"/>
		Frances<form:checkbox path="idiomaEscogido" value="Frances"/>
		Aleman<form:checkbox path="idiomaEscogido" value="Aleman"/>
	
	<br/><br/><br/>	

	Edad: 
	<form:input path="edad"/>	
	<form:errors path="edad" style="color:red"></form:errors>
	
	<br/><br/><br/>	
	
	Email: 
	<form:input path="email"/>	
	<form:errors path="email" style="color:red"></form:errors>
	
	<br/><br/><br/>	
	
	Codigo Postal: 
	<form:input path="codigoPostal"/>	
	<form:errors path="codigoPostal" style="color:red"></form:errors>
	
	<br/><br/><br/>	
	
	<input type="submit" value="Enviar">	
	</form:form>

</body>
</html>