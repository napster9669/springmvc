<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Alumno procesar formulario</title>
</head>
<body>

	El alumno con nombre ${nombreCompleto} 
	
	<br/>	
	
	y edad: ${alumno1.edad}
	
	<br/>

	La asignatura escodiga es ${alumno1.optativa}
	
	<br/>
	
	El alumno estudiará en ${alumno1.ciudadEstudio}
	
	<br/>	
	
	El alumno estudiará el/los idiomas ${alumno1.idiomaEscogido}
	
	<br/>	
	
	${nombreCompleto} con email:  ${alumno1.email} y codigo postal: ${alumno1.codigoPostal}
	
</body>
</html>